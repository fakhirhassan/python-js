def even_odd(number):
    if number % 2 == 0:
        return "Even"
    else:
        return "Odd"


while True:
    try:
        num = int(input("Enter a number "))
        if num == 'exit':
            break
        result = even_odd(num)
        print(f"{num} is {result}.")
    except ValueError:
        print("Invalid input. Please enter a valid number.")
