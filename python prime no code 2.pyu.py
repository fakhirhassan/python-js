def is_prime(number):
    if number <= 1:
        return False
    elif number <= 3:
        return True

    if number % 2 == 0 or number % 3 == 0:
        return False

    i = 5
    while i * i <= number:
        if number % i == 0 or number % (i + 2) == 0:
            return False
        i += 6

    return True


# Main program
while True:
    num = int(input("Enter a number (enter 0 to exit): "))
    if num == 0:
        break

    if is_prime(num):
        print(f"{num} is a prime number")
    else:
        print(f"{num} is not a prime number")
