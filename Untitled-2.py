def celcius_to_farenheit(celcius):
    farenheit = (celcius * 9/5) + 32
    return farenheit


def farenheit_to_celsius(farenheit):
    celsius = (farenheit - 32) * 5/9
    return celsius


while True:
    choice = int(
        input("\n1. Enter Celsius\n2. Enter Fahrenheit\nChoose any number (1/2): "))

    if choice == 1:
        celcius = float(input("Enter temperature in Celsius: "))
        fahrenheit = celcius_to_farenheit(celcius)
        print(
            f"The given temperature in Celsius is {celcius} and its conversion to Fahrenheit is {fahrenheit:.2f}")

    elif choice == 2:
        fahrenheit = float(input("Enter temperature in Fahrenheit: "))
        celsius = farenheit_to_celsius(fahrenheit)
        print(
            f"The given temperature in Fahrenheit is {fahrenheit} and its conversion to Celsius is {celsius:.2f}")

    else:
        print("Invalid number")
        break
