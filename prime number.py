number = int(input("write any number:"))
prime_number = True  # Assume the number is prime initially

if number <= 1:
    prime_number = False
else:
    for i in range(2, int(number ** 0.5) + 1):
        if number % i == 0:
            prime_number = False
            break

if prime_number:
    print(f"{number} is a prime number")
else:
    print(f"{number} is not a prime number")
