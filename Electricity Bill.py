from functools import reduce


total = [
    {
        "name": "officeAc",
        "avgPower": 2.2,
        "peakPower": 6,
        "dailyAvgUsage": 6,
        "dailyPeakUsage": 0.5,
    },
    {
        "name": "hallAc",
        "avgPower": 1.6,
        "peakPower": 10,
        "dailyAvgUsage": 12,
        "dailyPeakUsage": 0.2,
    },
    {
        "name": "kidsAc",
        "avgPower": 2.6,
        "peakPower": 8,
        "dailyAvgUsage": 8,
        "dailyPeakUsage": 0.5,
    },
    {
        "name": "familyRoom",
        "avgPower": 1.8,
        "peakPower": 10,
        "dailyAvgUsage": 11,
        "dailyPeakUsage": 0.5,
    }
]

averageUnitPrice = 42.72
overAllBill = 0


def totalUnitofAnAC(item):
    avgUnit = (item["avgPower"] * 230) / 1000
    peekUnit = (item["peakPower"] * 230) / 1000
    avgUnitsPerDay = avgUnit * item["dailyAvgUsage"]
    peekUnitsPerDay = peekUnit * item["dailyPeakUsage"]

    totalDailyUnits = avgUnitsPerDay + peekUnitsPerDay
    return totalDailyUnits


# for item in total:
#     totalDailyUnits = totalUnitofAnAC(item)
#     totalMonthlyUnits = totalDailyUnits * 30
#     totalBill = averageUnitPrice * totalMonthlyUnits
#     overAllBill += totalBill

#     print(f"Name: {item['name']}")
#     print(f"Total Daily Units: {totalDailyUnits}")
#     print(f"Total Monthly Units: {totalMonthlyUnits}")
#     print(f"Total Bill for the Month: {totalBill}")
#     print("-------------------")
arslanIsTraveling = False


def checkItem(item):
    return item["name"] == "familyRoom" and arslanIsTraveling


def applyMultipler(item):
    return (0.7, 0)[checkItem(item)]


def mapper(item):
    return {
        "name": item["name"],
        "avgPower": item["avgPower"],
        "peakPower": item["peakPower"],
        "dailyAvgUsage": item["dailyAvgUsage"] * applyMultipler(item),
        "dailyPeakUsage": item["dailyPeakUsage"] * applyMultipler(item),
    }


newListOfAcs = map(mapper, total)

totalUnits = reduce(lambda x, y: totalUnitofAnAC(y) + x, newListOfAcs, 0)


print(f"total overall Units are {totalUnits * 30} ")
print(f"total overall bill is {totalUnits * 30 * averageUnitPrice}")
