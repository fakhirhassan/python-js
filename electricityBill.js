var total = [
  {
    name: "officeAc",
    avgPower: 2.2,
    peakPower: 6,
    dailyAvgUsage: 6,
    dailyPeakUsage: 0.5,
  },
  {
    name: "hallAc",
    avgPower: 1.6,
    peakPower: 10,
    dailyAvgUsage: 12,
    dailyPeakUsage: 0.2,
  },
  {
    name: "kidsAc",
    avgPower: 2.6,
    peakPower: 8,
    dailyAvgUsage: 8,
    dailyPeakUsage: 0.5,
  },
  {
    name: "familyRoom",
    avgPower: 1.8,
    peakPower: 10,
    dailyAvgUsage: 11,
    dailyPeakUsage: 0.5,
  },
];

var accumalation = total.reduce(
  (pv, cv) => {
    return {
      avgPower: pv.avgPower + cv.avgPower,
      peakPower: pv.peakPower + cv.peakPower,
      dailyAvgUsage: pv.dailyAvgUsage + cv.dailyAvgUsage,
      dailyPeakUsage: pv.dailyPeakUsage + cv.dailyPeakUsage,
    };
  },
  { avgPower: 0, peakPower: 0, dailyAvgUsage: 0, dailyPeakUsage: 0 }
);

console.log("Accumulated AvgPower: ", accumalation);
